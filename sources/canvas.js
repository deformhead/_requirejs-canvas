define(

    function module( ) {

        'use strict' ;

        var Canvas ;

        Canvas = function Canvas( data ) {

            var initialize ;

            initialize = function initialize( data ) {

                // define canvas
                this.canvas = document.querySelector( data.selector ) ;

                this.canvas.width = data.width ;
                this.canvas.height = data.height ;

                // define canvas' context
                this.context = this.canvas.getContext( data.context ) ;

            // keep the context
            }.bind( this ) ;

            // initialize this instance
            initialize( data ) ;
        } ;

        return ( Canvas ) ;
    }
) ;
